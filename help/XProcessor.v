//=========================================================
// Jednocyklovy procesor.
//=========================================================

module CPU(
    clk,
    rst_n
);

  input  clk, rst_n;

  wire Zero, RegWrite, RegDst, ALUSrc, PCSrc, MemWrite, MemToReg;
  wire [2:0] ALUControl;
  wire [31:0] Instr, Rs_data, Rt_data, Rd_data, MuxAlu_data, ALUOut,
              SignExtendAlu_data, SignExtendAlu_data_sl2, Read_data,
              PCAddr, PCAddrPC, PCBranch, PCPlus4;
  wire [4:0] Rd_addr;

  assign SignExtendAlu_data_sl2 = SignExtendAlu_data << 2;
  
  // Prgramovy citac
  
  PC pc(
    .clk    (clk),
    .rst_n  (rst_n),
    .pc_in  (PCAddr),
    .pc_out (PCAddrPC)
  );
  
  // Instrukcni pamet
  
  Sum sum_imemory_plus4(
    .data1_in (PCAddrPC),
    .data2_in (32'b00000000000000000000000000000100),
    .data_out (PCPlus4)
  );
  
  Instr_Memory imemory(
    .addr  (PCAddrPC),
    .instr (Instr)
  );
  
  Mux2x1_32bit mux_pc(
    .data0_in (PCPlus4),
    .data1_in (PCBranch),
    .select   (PCSrc),
    .data_out (PCAddr) 
  );
  
  Sum sum_imemory_sign(
    .data1_in (SignExtendAlu_data_sl2),
    .data2_in (PCPlus4),
    .data_out (PCBranch)
  );
  
  // Ridici jednotka

  Control_Unit cunit(
    .Opcode     (Instr[31:26]),
    .Funct      (Instr[5:0]),
    .Zero       (Zero),
    .RegWrite   (RegWrite),
    .RegDst     (RegDst),
    .ALUSrc     (ALUSrc),
    .PCSrc      (PCSrc),
    .MemWrite   (MemWrite),
    .MemToReg   (MemToReg),
    .ALUControl (ALUControl)
  );
  
  // Registry
  
  Registers regs(
    .clk      (clk),
    .Rs_addr  (Instr[25:21]),
    .Rt_addr  (Instr[20:16]),
    .Rd_addr  (Rd_addr), 
    .Rd_data  (Rd_data),
    .RegWrite (RegWrite), 
    .Rs_data  (Rs_data), 
    .Rt_data  (Rt_data)
  );
  
  Mux2x1_5bit mux_regs(
    .data0_in (Instr[20:16]),
    .data1_in (Instr[15:11]),
    .select   (RegDst),
    .data_out (Rd_addr)
  );
  
  // ALU
  
  Mux2x1_32bit mux_alu(
    .data0_in (Rt_data),
    .data1_in (SignExtendAlu_data),
    .select   (ALUSrc),
    .data_out (MuxAlu_data)
  );
  
  Alu alu(
    .SrcA       (Rs_data),
    .SrcB       (MuxAlu_data),
    .ALUControl (ALUControl),
    .ALUOut     (ALUOut),
    .Zero       (Zero)
  );

  Sign_Extend sextend_alu(
    .data_in  (Instr[15:0]),
    .data_out (SignExtendAlu_data)
  );
  
  // Datova pamet
  
  Data_Memory dmemory(
    .clk          (clk),
    .Addr         (ALUOut),
    .Write_data   (Rt_data),
    .Write_enable (MemWrite),
    .Read_data    (Read_data)
  );
  
  Mux2x1_32bit mux_dmemory(
    .data0_in (ALUOut),
    .data1_in (Read_data),
    .select   (MemToReg),
    .data_out (Rd_data)
  );
  
endmodule





//=========================================================
// HLAVNI RIDICI JEDNOTKA (ridici signaly)
//INPUT: Opcode, Funct Zero
//Output: MemToReg, MemWrite, PCSrc, ALUControl, ALUSrc...
//        RegDst, RegWrite

//=========================================================
// Ridici jednotka.
//=========================================================

module Control_Unit(
  Opcode,
  Funct,
  Zero,
  RegWrite,
  RegDst,
  ALUSrc,
  PCSrc,
  MemWrite,
  MemToReg,
  ALUControl
);

  input  [5:0] Opcode;
  input  [5:0] Funct;
  input        Zero;
  output       RegWrite;
  output       RegDst;
  output       ALUSrc;
  output       PCSrc;
  output       MemWrite;
  output       MemToReg;
  output [2:0] ALUControl;	 
  
  wire   [1:0] ALUOp;
  wire         Branch;	 

  Main_Dec mdec(Opcode, RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg);
  Alu_Dec  adec(ALUOp, Funct, ALUControl);	 
  
  assign PCSrc = Zero & Branch;
endmodule

//=========================================================
// Dekoder ALU.
//=========================================================

module Alu_Dec(
  ALUOp,
  Funct,
  ALUControl
);

  input  [1:0] ALUOp;
  input  [5:0] Funct;
  output [2:0] ALUControl;
  
  reg    [2:0] ALUControl;

  always @(*)
    case(ALUOp)
      2'b00: ALUControl = 3'b010;
      2'b01: ALUControl = 3'b110;
    default:
      case(Funct)
        6'b100000: ALUControl = 3'b010;
        6'b100010: ALUControl = 3'b110;
        6'b100100: ALUControl = 3'b000;
        6'b100101: ALUControl = 3'b001;
        6'b101010: ALUControl = 3'b111;
      endcase
    endcase

endmodule


//=========================================================
// Hlavni jednotka.
//=========================================================

module Main_Dec(
  Opcode,
  RegWrite,
  RegDst,
  ALUSrc,
  ALUOp,
  Branch,
  MemWrite,
  MemToReg
);

  input  [5:0] Opcode;
  output       RegWrite;
  output       RegDst;
  output       ALUSrc;
  output [1:0] ALUOp;
  output       Branch;
  output       MemWrite;
  output       MemToReg;

  reg    [7:0] out = 0;

  always @ (Opcode)
    case(Opcode)
      6'b000000 : out = 8'b11010000;
      6'b100011 : out = 8'b10100001;
      6'b101011 : out = 8'b00100010;
      6'b000100 : out = 8'b00001100;
      6'b001000 : out = 8'b10100000;
      6'b000101 : out = 8'b00001100;
    endcase

  assign {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg} = out;

endmodule
//=========================================================



//=========================================================
// Program counter (PC).
//=========================================================

module PC(
  clk,
  rst_n,
  pc_in,
  pc_out
);

  input         clk;
  input         rst_n;
  input  [31:0] pc_in;
  output [31:0] pc_out;

  reg    [31:0] pc_out;

  always @(posedge clk or negedge rst_n)
  begin
    if (~rst_n)
      begin
        pc_out = 32'b0;
      end
    else
      begin
        pc_out = pc_in;
      end
  end

endmodule


//=========================================================
// Instrukcni pamet.
// 
// Typu ROM pevne velkosti (1 kB), obsah ze souboru
//                                 "instr_memory.txt"
//=========================================================

module Instr_Memory(
  addr,
  instr
);

  input  [31:0] addr;  
  output [31:0] instr;

  reg    [31:0] ROM[0:255];

  initial
    begin
      $readmemh("instr_memory.txt", ROM);
    end

  assign instr = ROM[addr >> 2];

endmodule


//=========================================================
// Tri portovy soubor 32 32-bitovych registru.
//=========================================================

module Registers(
  clk,
  Rs_addr,
  Rt_addr,
  Rd_addr, 
  Rd_data,
  RegWrite, 
  Rs_data, 
  Rt_data 
);

  input         clk;
  input  [4:0]  Rs_addr; // 5-bitova adresa pro cteni Rs_data
  input  [4:0]  Rt_addr; // 5-bitova adresa pro cteni Rt_data
  input  [4:0]  Rd_addr; // 5-bitova adresa pro zapis Rd_data
  input  [31:0] Rd_data;
  input         RegWrite;
  output [31:0] Rs_data; 
  output [31:0] Rt_data;

  reg    [31:0] register [0:31]; // 32 32-bitovych registru

  // Cteni dat - pokud je adresa 0, vzdy je vystup 0
  assign Rs_data = (Rs_addr == 5'b0) ? 32'b0 : register[Rs_addr];
  assign Rt_data = (Rt_addr == 5'b0) ? 32'b0 : register[Rt_addr];

  // Zapis dat
  always @ (posedge clk)
  begin
    if(RegWrite)
      register[Rd_addr] = Rd_data;
  end
   
endmodule 


//=========================================================
// ALU (operace nad operandama)
// 32-bit ALU
// Operations: ALUControl = 010 => add
//             ALUControl = 110 => sub
//             ALUControl = 000 => and
//             ALUControl = 001 => or
//             ALUControl = 011 => xor
//             ALUControl = 111 => slt (set less than)


module Alu(
  SrcA,
  SrcB,
  ALUControl,
  ALUOut,
  Zero
);	 

  input  [31:0] SrcA;
  input  [31:0] SrcB;
  input  [2:0]  ALUControl;
  output [31:0] ALUOut;
  output        Zero;

  reg    [31:0] out = 0;

  always @ (*)
    case (ALUControl)
      3'b010: out = SrcA + SrcB;
      3'b110: out = SrcA - SrcB;
      3'b000: out = SrcA & SrcB;
      3'b001: out = SrcA | SrcB;
      3'b011: out = SrcA ^ SrcB;
      3'b111: out = (SrcA < SrcB) ? 1 : 0;
      default: out = 0;
    endcase

  assign ALUOut = out;
  assign Zero = (out == 0) ? 1 : 0;

endmodule



//=========================================================
// DATOVA PAMET
// 2x1 multiplexor (32-bit).
// 
// Bytove adresovatelna pamet, data jsou bytove-zarovnana
// Velikost: 32 Bytes
// Rozsah adres: 0x00 ~ 0x1F
//=========================================================

module Data_Memory(
  clk,
  Addr,
  Write_data,
  Write_enable,
  Read_data
);

  input         clk;
  input  [31:0] Addr;
  input  [31:0] Write_data;
  input         Write_enable;
  output [31:0] Read_data;

  reg    [7:0]  memory [0:31];

  // Zapis dat     
  always @(posedge clk)
    begin
      if (Write_enable)
        begin
          memory[Addr + 3] <= Write_data[31:24];
          memory[Addr + 2] <= Write_data[23:16];
          memory[Addr + 1] <= Write_data[15:8];
          memory[Addr]     <= Write_data[7:0];
        end
    end

  // Cteni dat
  assign  Read_data = {memory[Addr + 3], memory[Addr + 2], memory[Addr + 1], memory[Addr]};

endmodule


//=========================================================
// 2x1 multiplexor (32-bit).
//=========================================================

module Mux2x1_32bit(    
  data0_in,
  data1_in,
  select,
  data_out
);

  input  [31:0] data0_in, data1_in;
  input         select;
  output [31:0] data_out;

  assign data_out = (select) ? data1_in : data0_in;

endmodule



//=========================================================
// 2x1 multiplexor (5-bit).
//=========================================================

module Mux2x1_5bit(    
  data0_in,
  data1_in,
  select,
  data_out
);

  input  [4:0] data0_in, data1_in;
  input        select;
  output [4:0] data_out;

  assign data_out = (select) ? data1_in : data0_in;

endmodule


//=========================================================
// Znamenkove rozsireni 16-bitoveho cisla na 32-bitove.
//=========================================================

module Sign_Extend(
  data_in,
  data_out
);

  input  [15:0] data_in;
  output [31:0] data_out;

  assign data_out = {{16{data_in[15]}}, data_in};

endmodule


//=========================================================
// 32-bitovy sumator.
//=========================================================

module Sum(
  data1_in,
  data2_in,
  data_out
);

  input  [31:0] data1_in;
  input  [31:0] data2_in;
  output [31:0] data_out;

  reg    [31:0] data_out;

  always @ (data1_in or data2_in) begin
    data_out = data1_in + data2_in;
  end
  
endmodule
