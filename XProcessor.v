//=========================================================
// Jednocyklovy procesor.
//=========================================================

module CPU(
    clk,
    rst_n
);

  input  clk, rst_n;

  wire Zero, RegWrite, RegDst, ALUSrc, PCSrc, MemWrite, MemToReg, jalsel, Jump;
  wire [2:0] ALUControl;
  wire [31:0] Instr, Rs_data, Rt_data, Rd_data, Rd_select, MuxAlu_data, ALUOut,
              SignExtendAlu_data, SignExtendAlu_data_sl2, Read_data,
              PCAddr, PCAddrPC, PCBranch, PCPlus4, PCPlus8, PCJump, PCBranchJAL, JALaddr,
	      RDdataFinal;
  wire [4:0] Rd_addr, Rd_addr_tmp;

  assign SignExtendAlu_data_sl2 = SignExtendAlu_data << 2;
  
  // Prgramovy citac
  
  PC pc(
    .clk    (clk),
    .rst_n  (rst_n),
    .pc_in  (PCAddr),
    .pc_out (PCAddrPC)
  );
  
  // Instrukcni pamet
  
  Sum sum_imemory_plus4(
    .data1_in (PCAddrPC),
    .data2_in (32'b00000000000000000000000000000100),
    .data_out (PCPlus4)
  );
  
  Instr_Memory imemory(
    .addr  (PCAddrPC),
    .instr (Instr)
  );
  
  Mux2x1_32bit mux_pc(
    .data0_in (PCPlus4),
    .data1_in (PCJump),
    .select   (PCSrc),
    .data_out (PCAddr) 
  );
  
  Sum sum_imemory_sign(
    .data1_in (SignExtendAlu_data_sl2),
    .data2_in (PCPlus4),
    .data_out (PCBranch)
  );
  
  // Ridici jednotka

  Control_Unit cunit(
    .Opcode     (Instr[31:26]),
    .Funct      (Instr[5:0]),
    .Zero       (Zero),
    .RegWrite   (RegWrite),
    .RegDst     (RegDst),
    .ALUSrc     (ALUSrc),
    .PCSrc      (PCSrc),
    .MemWrite   (MemWrite),
    .MemToReg   (MemToReg),
    .ALUControl (ALUControl),
    .Jump       (Jump),
    .JAL        (jalsel)
  );
  
  // Registry
  
  Registers regs(
    .clk      (clk),
    .Rs_addr  (Instr[25:21]),
    .Rt_addr  (Instr[20:16]),
    .Rd_addr  (Rd_addr), 
    .Rd_data  (RDdataFinal),
    .RegWrite (RegWrite), 
    .Rs_data  (Rs_data), 
    .Rt_data  (Rt_data)
  );
  
  Mux2x1_5bit mux_regs(
    .data0_in (Instr[20:16]),
    .data1_in (Instr[15:11]),
    .select   (RegDst),
    .data_out (Rd_addr_tmp)
  );
  
  // ALU
  
  Mux2x1_32bit mux_alu(
    .data0_in (Rt_data),
    .data1_in (SignExtendAlu_data),
    .select   (ALUSrc),
    .data_out (MuxAlu_data)
  );
  
  Alu alu(
    .SrcA       (Rs_data),
    .SrcB       (MuxAlu_data),
    .ShftNum    (Instr[10:6]),
    .ALUControl (ALUControl),
    .ALUOut     (ALUOut),
    .Zero       (Zero)
  );

  Sign_Extend sextend_alu(
    .data_in  (Instr[15:0]),
    .data_out (SignExtendAlu_data)
  );
  
  // Datova pamet
  
  Data_Memory dmemory(
    .clk          (clk),
    .Addr         (ALUOut),
    .Write_data   (Rt_data),
    .Write_enable (MemWrite),
    .Read_data    (Read_data)
  );
  
  Mux2x1_32bit mux_dmemory(
    .data0_in (ALUOut),
    .data1_in (Read_data),
    .select   (MemToReg),
    .data_out (Rd_data)
  );
  
  //(PCbranch x JALaddr) x Rd_data
  Mux2x1_32bit jump_sel(
    .data0_in (PCBranchJAL),
    .data1_in (Rd_data),
    .select   (Jump),
    .data_out (PCJump)
  );

  Mux2x1_32bit jump_sel2(
    .data0_in (Rd_data),
    .data1_in (32'b00000000000000000000000000000000),
    .select   (Jump),
    .data_out (Rd_select)
  );

/////////////////////////////////////////////////////////////////////

  PCJAL pcjal1(
    .PC_in (PCAddrPC),
    .Instr_in (Instr[25:0]),
    .PC_out (JALaddr)
  );

  //JAL or PC branch
  Mux2x1_32bit mux_JALbranch(
    .data0_in (PCBranch),
    .data1_in (JALaddr),
    .select   (jalsel),
    .data_out (PCBranchJAL)
  );

  Mux2x1_32bit mux_JALbranch2(
    .data0_in (Rd_select),
    .data1_in (PCPlus8),
    .select   (jalsel),
    .data_out (RDdataFinal)
  );
  
  Mux2x1_5bit mux_regs2(
    .data0_in (Rd_addr_tmp),
    .data1_in (5'd31),
    .select   (jalsel),
    .data_out (Rd_addr)
  );

  Sum sum_imemory_plus8(
    .data1_in (PCAddrPC),
    .data2_in (32'b00000000000000000000000000001000),
    .data_out (PCPlus8)
  );

  
endmodule





//=========================================================
// HLAVNI RIDICI JEDNOTKA (ridici signaly)
//INPUT: Opcode, Funct Zero
//Output: MemToReg, MemWrite, PCSrc, ALUControl, ALUSrc...
//        RegDst, RegWrite

//=========================================================
// Ridici jednotka.
//=========================================================

module Control_Unit(
  Opcode,
  Funct,
  Zero,
  RegWrite,
  RegDst,
  ALUSrc,
  PCSrc,
  MemWrite,
  MemToReg,
  ALUControl,
  Jump,
  JAL
);

  input  [5:0] Opcode;
  input  [5:0] Funct;
  input        Zero;
  output       RegWrite;
  output       RegDst;
  output       ALUSrc;
  output       PCSrc;
  output       MemWrite;
  output       MemToReg;
  output [2:0] ALUControl;
  output       Jump;	 
  output       JAL;
  
  wire   [1:0] ALUOp;
  wire   [1:0] Branch;
  
  reg PCSrc;	
  reg Jump; 

  Main_Dec mdec(Opcode, RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, JAL);
  Alu_Dec  adec(ALUOp, Funct, ALUControl);	 
  
  always @ (*)
    case (Branch)
      2'b10 :               //bne
        begin 
          PCSrc = Zero?0:1; 
          Jump = 0;
        end
      2'b01 :               //beq
        begin
           PCSrc = Zero?1:0;  
           Jump = 0;
        end
      2'b11 :               // Jump -- Tohle je nesmysl, jelikoz JR ma opCode 00000 ale ostatni R instrukce neskacou
        begin  
           PCSrc = 1;    
           Jump = 1;
        end
      default :            // other
        begin
          //=== Pokud je R instrukci s funkci 00 1000 - pak je to JR a tim padem skacu
		  if( Opcode == 0 && Funct == 8 )
			begin
				Jump = 1;
				PCSrc = 1;
			end
		  else
			begin
          		Jump = 0;
				PCSrc = JAL; 
			end
        end 
    endcase
endmodule

//=========================================================
// Dekoder ALU.
//=========================================================

module Alu_Dec(
  ALUOp,
  Funct,
  ALUControl
);

  input  [1:0] ALUOp;
  input  [5:0] Funct;
  output [2:0] ALUControl;
  
  reg    [2:0] ALUControl;

  always @(*)
    case(ALUOp)
      2'b00: ALUControl = 3'b010;
      2'b01: ALUControl = 3'b110;
    default:
      case(Funct)
        6'b100000: ALUControl = 3'b010; //ADD
        6'b100010: ALUControl = 3'b110; //SUB
        6'b100100: ALUControl = 3'b000; //AND
        6'b100101: ALUControl = 3'b001; //OR
        6'b101010: ALUControl = 3'b111; //SLT
        6'b000000: ALUControl = 3'b011; //SLL
        6'b000010: ALUControl = 3'b100; //SLR
        6'b001000: ALUControl = 3'b101; //JR
      endcase
    endcase

endmodule


//=========================================================
// Hlavni jednotka.
//=========================================================

module Main_Dec(
  Opcode,
  RegWrite,
  RegDst,
  ALUSrc,
  ALUOp,
  Branch,
  MemWrite,
  MemToReg,
  JAL
);

  input  [5:0] Opcode;
  output       RegWrite;
  output       RegDst;
  output       ALUSrc;
  output [1:0] ALUOp;
  output [1:0] Branch;
  output       MemWrite;
  output       MemToReg;
  output       JAL;

  reg    [9:0] out = 0;

  always @ (Opcode)
    case(Opcode)
      6'b000000 : out = 10'b1101000000;
      6'b100011 : out = 10'b1010000010;
      6'b101011 : out = 10'b0010000100;
      6'b000100 : out = 10'b0000101000;     //beq
      6'b000101 : out = 10'b0000110000;     //bne
      6'b001000 : out = 10'b1010000000;
      6'b000011 : out = 10'b1000000001; //JAL
    endcase

  assign {RegWrite, RegDst, ALUSrc, ALUOp, Branch, MemWrite, MemToReg, JAL} = out;

endmodule
//=========================================================



//=========================================================
// Program counter (PC).
//=========================================================

module PC(
  clk,
  rst_n,
  pc_in,
  pc_out
);

  input         clk;
  input         rst_n;
  input  [31:0] pc_in;
  output [31:0] pc_out;

  reg    [31:0] pc_out;

  always @(posedge clk or negedge rst_n)
  begin
    if (~rst_n)
      begin
        pc_out = 32'b0;
      end
    else
      begin
        pc_out = pc_in;
      end
  end

endmodule


//=========================================================
// Instrukcni pamet.
// 
// Typu ROM pevne velkosti (1 kB), obsah ze souboru
//                                 "instr_memory.txt"
//=========================================================

module Instr_Memory(
  addr,
  instr
);

  input  [31:0] addr;  
  output [31:0] instr;

  reg    [31:0] ROM[0:255];

  initial
    begin
      	$readmemh("testHEX", ROM);
		//$readmemh("instr_memory.txt", ROM);
    end

  assign instr = ROM[addr >> 2];

endmodule


//=========================================================
// Tri portovy soubor 32 32-bitovych registru.
//=========================================================

module Registers(
  clk,
  Rs_addr,
  Rt_addr,
  Rd_addr, 
  Rd_data,
  RegWrite, 
  Rs_data, 
  Rt_data 
);

  input         clk;
  input  [4:0]  Rs_addr; // 5-bitova adresa pro cteni Rs_data
  input  [4:0]  Rt_addr; // 5-bitova adresa pro cteni Rt_data
  input  [4:0]  Rd_addr; // 5-bitova adresa pro zapis Rd_data
  input  [31:0] Rd_data;
  input         RegWrite;
  output [31:0] Rs_data; 
  output [31:0] Rt_data;

  reg    [31:0] register [0:31]; // 32 32-bitovych registru

  // Cteni dat - pokud je adresa 0, vzdy je vystup 0
  assign Rs_data = (Rs_addr == 5'b0) ? 32'b0 : register[Rs_addr];
  assign Rt_data = (Rt_addr == 5'b0) ? 32'b0 : register[Rt_addr];

  // Zapis dat
  always @ (posedge clk)
  begin
    if(RegWrite)
      register[Rd_addr] = Rd_data;
  end
   
endmodule 


//=========================================================
// ALU (operace nad operandama)
// 32-bit ALU
// Operations: ALUControl = 010 => add
/*      ALUControl = 3'b010; //ADD
        ALUControl = 3'b110; //SUB
        ALUControl = 3'b000; //AND
        ALUControl = 3'b001; //OR
        ALUControl = 3'b111; //SLT
        ALUControl = 3'b011; //SLL
        ALUControl = 3'b100; //SLR
        ALUControl = 3'b101; //JR
*/

module Alu(
  SrcA,
  SrcB,
  ShftNum,
  ALUControl,
  ALUOut,
  Zero
);	 

  input  [31:0] SrcA;
  input  [31:0] SrcB;
  input  [4:0]  ShftNum;
  input  [2:0]  ALUControl;
  output [31:0] ALUOut;
  output        Zero;

  reg    [31:0] out = 0;

  always @ (*)
    case (ALUControl)
      3'b010: out = SrcA + SrcB;
      3'b110: out = SrcA - SrcB;
      3'b000: out = SrcA & SrcB;
      3'b001: out = SrcA | SrcB;
      3'b011: out = SrcB << ShftNum;
      3'b100: out = SrcB >> ShftNum;
      3'b101: out = SrcA;
      3'b111: out = (SrcA < SrcB) ? 1 : 0;
      default: out = 0;
    endcase

  assign ALUOut = out;
  assign Zero = (out == 0) ? 1 : 0;

endmodule



//=========================================================
// DATOVA PAMET
// 2x1 multiplexor (32-bit).
// 
// Bytove adresovatelna pamet, data jsou bytove-zarovnana
// Velikost: 32 Bytes
// Rozsah adres: 0x00 ~ 0x1F
//=========================================================

module Data_Memory(
  clk,
  Addr,
  Write_data,
  Write_enable,
  Read_data
);

  input         clk;
  input  [31:0] Addr;
  input  [31:0] Write_data;
  input         Write_enable;
  output [31:0] Read_data;

  reg    [7:0]  memory [0:255];

  
  // Zapis dat     
  always @(posedge clk)
    begin
      if (Write_enable)
        begin
			memory[Addr + 31] <= Write_data[255:248];	        
			memory[Addr + 30] <= Write_data[247:240];	        
			memory[Addr + 29] <= Write_data[239:232];	        
			memory[Addr + 28] <= Write_data[231:224];	        
			memory[Addr + 27] <= Write_data[223:216];	        
			memory[Addr + 26] <= Write_data[215:208];	        
			memory[Addr + 25] <= Write_data[207:200];	        
			memory[Addr + 24] <= Write_data[199:192];	        
			memory[Addr + 23] <= Write_data[191:184];	        
			memory[Addr + 22] <= Write_data[183:176];	        
			memory[Addr + 21] <= Write_data[175:168];	        
			memory[Addr + 20] <= Write_data[167:160];	        
			memory[Addr + 19] <= Write_data[159:152];	        
			memory[Addr + 18] <= Write_data[151:144];	        
			memory[Addr + 17] <= Write_data[143:136];
	        memory[Addr + 16] <= Write_data[135:128];
	        memory[Addr + 15] <= Write_data[127:120];
	        memory[Addr + 14] <= Write_data[119:112];
	        memory[Addr + 13] <= Write_data[111:104];
	        memory[Addr + 12] <= Write_data[103:96];
	        memory[Addr + 11] <= Write_data[95:88];
	        memory[Addr + 10] <= Write_data[87:80];
	        memory[Addr + 9] <= Write_data[79:72];
	        memory[Addr + 8] <= Write_data[71:64];
	        memory[Addr + 7] <= Write_data[63:56];
	        memory[Addr + 6] <= Write_data[55:48];
	        memory[Addr + 5] <= Write_data[47:40];
	        memory[Addr + 4] <= Write_data[39:32];
	        memory[Addr + 3] <= Write_data[31:24];
	        memory[Addr + 2] <= Write_data[23:16];
			memory[Addr + 1] <= Write_data[15:8];
			memory[Addr    ] <= Write_data[7:0];
        end
    end

  // Cteni dat
  assign  Read_data = {memory[Addr + 3], memory[Addr + 2], memory[Addr + 1], memory[Addr]};

endmodule


//=========================================================
// 2x1 multiplexor (32-bit).
//=========================================================

module Mux2x1_32bit(    
  data0_in,
  data1_in,
  select,
  data_out
);

  input  [31:0] data0_in, data1_in;
  input         select;
  output [31:0] data_out;

  assign data_out = (select) ? data1_in : data0_in;

endmodule



//=========================================================
// 2x1 multiplexor (5-bit).
//=========================================================

module Mux2x1_5bit(    
  data0_in,
  data1_in,
  select,
  data_out
);

  input  [4:0] data0_in, data1_in;
  input        select;
  output [4:0] data_out;

  assign data_out = (select) ? data1_in : data0_in;

endmodule


//=========================================================
// Znamenkove rozsireni 16-bitoveho cisla na 32-bitove.
//=========================================================

module Sign_Extend(
  data_in,
  data_out
);

  input  [15:0] data_in;
  output [31:0] data_out;

  assign data_out = {{16{data_in[15]}}, data_in};

endmodule


//=========================================================
// 32-bitovy sumator.
//=========================================================

module Sum(
  data1_in,
  data2_in,
  data_out
);

  input  [31:0] data1_in;
  input  [31:0] data2_in;
  output [31:0] data_out;

  reg    [31:0] data_out;

  always @ (data1_in or data2_in) begin
    data_out = data1_in + data2_in;
  end
  
endmodule

//==========================================================
// PC Jump
//==========================================================
  
module PCJAL(
	PC_in,
	Instr_in,
	PC_out	
);

	input	[31:0] PC_in;
	input	[25:0] Instr_in;
	output	[31:0] PC_out;

	reg	[31:0] PC_out;
	
	always @ (*) begin
		PC_out = (PC_in & 32'hf0000000) | (Instr_in << 2);
	end

endmodule

















