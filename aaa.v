module test();
  reg[5:0] OpCode, Funct;
  reg Zero;
  wire RegWrite, RegDst,AluSrc,PCSrc,MemWrite, MemToReg;
  wire[2:0] ALUControl;
  
  Control_Unit cu(OpCode, Funct, Zero, RegWrite, RegDst,AluSrc,PCSrc,MemWrite, MemToReg,ALUControl);
  
  initial begin
    $dumpfile("test");
    $dumpvars;
    OpCode=6'b000100; //beq
    Funct=6'b001000;
    Zero=0;
    #320 $finish;
  end
  
  always #120 OpCode=6'b000101; //bne
  always #80 Zero = ~Zero;
  
  always @(*) $display( "PCSrc= %d %d", PCSrc,OpCode);
  
endmodule
