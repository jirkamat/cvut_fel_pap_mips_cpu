`define CYCLE_TIME 50
`timescale 1ns / 1ps

//=========================================================
// Test procesoru - jednocyklovy procesor.
//=========================================================

module CPUTest;

  // Inputs
  reg clk;
  reg rst_n;
  integer i, outfile, counter;

  always #(`CYCLE_TIME / 2) clk = ~clk;

  // CPU
  CPU CPU (
   .clk(clk), 
   .rst_n(rst_n)
  );

  initial begin
    $dumpfile("test");
    $dumpvars;
    // Inicializace datove pameti
    for(i = 0; i < 256; i = i + 1) begin
      CPU.dmemory.memory[i] = 8'h0;
    end    

    // Inicializace pole registru
    for(i = 0; i < 32; i = i + 1) begin
      CPU.regs.register[i] = 32'b0;
    end

    // Vystup
    outfile = $fopen("cmdline.txt") | 1;

    // Nastaveni pameti
    CPU.dmemory.memory[16] = 8'd99; // a
    CPU.dmemory.memory[20] = 8'd45; // b
    CPU.dmemory.memory[24] = 8'h1; // c

    counter = 0;
    clk = 0;
    rst_n = 0;

    #(`CYCLE_TIME / 4) 
    rst_n = 1;

  end

  always @ (posedge clk) begin
    if (counter == 500) // zastavi po 50 cyklech
      $finish;

    $fdisplay(outfile, "///////////////////////////////////////////////////////////////////////////");

    // print regs
    $fdisplay(outfile, "Registers:");
    $fdisplay(outfile, "R0 = %d, R8  = %d, R16 = %d, R24 = %d", CPU.regs.register[0], CPU.regs.register[8] , CPU.regs.register[16], CPU.regs.register[24]);
    $fdisplay(outfile, "R1 = %d, R9  = %d, R17 = %d, R25 = %d", CPU.regs.register[1], CPU.regs.register[9] , CPU.regs.register[17], CPU.regs.register[25]);
    $fdisplay(outfile, "R2 = %d, R10 = %d, R18 = %d, R26 = %d", CPU.regs.register[2], CPU.regs.register[10], CPU.regs.register[18], CPU.regs.register[26]);
    $fdisplay(outfile, "R3 = %d, R11 = %d, R19 = %d, R27 = %d", CPU.regs.register[3], CPU.regs.register[11], CPU.regs.register[19], CPU.regs.register[27]);
    $fdisplay(outfile, "R4 = %d, R12 = %d, R20 = %d, R28 = %d", CPU.regs.register[4], CPU.regs.register[12], CPU.regs.register[20], CPU.regs.register[28]);
    $fdisplay(outfile, "R5 = %d, R13 = %d, R21 = %d, R29 = %d", CPU.regs.register[5], CPU.regs.register[13], CPU.regs.register[21], CPU.regs.register[29]);
    $fdisplay(outfile, "R6 = %d, R14 = %d, R22 = %d, R30 = %d", CPU.regs.register[6], CPU.regs.register[14], CPU.regs.register[22], CPU.regs.register[30]);
    $fdisplay(outfile, "R7 = %d, R15 = %d, R23 = %d, R31 = %d", CPU.regs.register[7], CPU.regs.register[15], CPU.regs.register[23], CPU.regs.register[31]);

    // print dmemory
    $fdisplay(outfile, "Data Memory:");
	for(i = 0; i < 160; i = i + 4) begin
      $fdisplay(outfile, "0x%2x =%d",i, {
				CPU.dmemory.memory[i+3] , 
				CPU.dmemory.memory[i+2] , 
				CPU.dmemory.memory[i+1] , 			
				CPU.dmemory.memory[i] 	});
    end

    $fdisplay(outfile, "");

    // print statuses
    $fdisplay(outfile, "PC = %d", CPU.pc.pc_out);
    $fdisplay(outfile, "Next PC = %d (select %b, data0 = %d, data1 = %d)", CPU.mux_pc.data_out, CPU.mux_pc.select, CPU.mux_pc.data0_in, CPU.mux_pc.data1_in);
    $fdisplay(outfile, "Instruction = %b", CPU.imemory.instr);

    $fdisplay(outfile, "Control unit: [INPUT] Opcode = %b, Funct = %b, Zero = %b",CPU.cunit.Opcode, CPU.cunit.Funct, CPU.cunit.Zero);

    $fdisplay(outfile, "Control unit: [OUTPUT] RegWrite = %b; RegDst = %b, ALUSrc = %b, PCSrc = %b, MemWrite = %b, MemToReg = %b, ALUControl = %b", CPU.cunit.RegWrite, CPU.cunit.RegDst, CPU.cunit.ALUSrc, CPU.cunit.PCSrc, CPU.cunit.MemWrite, CPU.cunit.MemToReg, CPU.cunit.ALUControl);
    $fdisplay(outfile, "ALU: [INPUT] SrcA = %b; SrcB = %b, ALUControl = %b", CPU.alu.SrcA, CPU.alu.SrcB, CPU.alu.ALUControl);
    $fdisplay(outfile, "ALU: [OUTPUT] ALUOut = %b, Zero = %b", CPU.alu.ALUOut, CPU.alu.Zero);

    //$fdisplay(outfile, "\n");

    counter = counter + 1;
  end

endmodule
